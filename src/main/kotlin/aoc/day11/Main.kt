package aoc.day11

import aoc.day10.Point
import aoc.day7.IntCodeComputer
import aoc.day7.IntCodeComputerState
import shared.FileReader

const val black = 0
const val white = 1
const val up = 0
const val right = 1
const val down = 2
const val left = 3
const val leftTurn = 0
const val rightTurn = 1

fun main() {
    val codes = FileReader().readLongArray("day11_input.txt")
    val panels = mutableMapOf<Point, Int>()
    runRobot(codes, panels)
    println(panels.size)

    val panelsStartingWithWhite = mutableMapOf<Point, Int>()
    panelsStartingWithWhite.put(Point(0, 0), white)
    runRobot(codes, panelsStartingWithWhite)
    printPanels(panelsStartingWithWhite)
}

fun runRobot(codes: List<Long>, panels: MutableMap<Point, Int>) {
    var currentPos = Point(0, 0)
    var currentDirection = up

    val state = IntCodeComputerState(
        codes.toMutableList(),
        mutableListOf(),
        mutableListOf(),
        0,
        false,
        0,
        -1,
        0
    )
    val computer = IntCodeComputer(state)

    while (!computer.state.isDone) {
        computer.state.inputs = mutableListOf(panels.getOrPut(currentPos, { black }).toLong())
        computer.runUntilNOutputsOrDone(2)
        if (state.outputs.isNotEmpty()) {
            val newColour = computer.state.outputs[0]
            val turnDirection = computer.state.outputs[1]
            panels.put(currentPos, newColour.toInt())
            currentDirection = newDirection(currentDirection, turnDirection.toInt())
            currentPos = newPos(currentPos, currentDirection)
            state.outputs.clear()
        }
    }
}

private fun printPanels(panels: MutableMap<Point, Int>) {
    val sortedXComponents = panels.keys.map { it.x }.sorted()
    val sortedYComponents = panels.keys.map { it.y }.sorted()
    val startX = sortedXComponents.first()
    val endX = sortedXComponents.last()
    val startY = sortedYComponents.last()
    val endY = sortedYComponents.first()

    for (row in startY downTo endY) {
        for (col in startX..endX) {
            val char = panels.getOrDefault(Point(col, row), black)
            print(if (char == 1) 'O' else ' ')
        }
        println()
    }
}

private fun newPos(currentPos: Point, currentDirection: Int): Point {
    return when (currentDirection) {
        up -> Point(currentPos.x, currentPos.y + 1)
        right -> Point(currentPos.x + 1, currentPos.y)
        down -> Point(currentPos.x, currentPos.y - 1)
        left -> Point(currentPos.x - 1, currentPos.y)
        else -> throw RuntimeException("Unknown direction Code $currentDirection")
    }
}

private fun newDirection(direction: Int, turnDirection: Int): Int {
    return when (turnDirection) {
        leftTurn -> if (direction - 1 < 0) direction - 1 + 4 else direction - 1
        rightTurn -> if (direction + 1 > 3) direction + 1 - 4 else direction + 1
        else -> throw RuntimeException("Unknown turn direction code $turnDirection")
    }
}