package aoc.day8

import shared.FileReader

fun main() {
    val layers = FileReader().readArrays("day8_input.txt", 25 * 6)
    val layerWithLeastZeros = findLayerWithLeastZeros(layers)
    val result = calculateMultipleOfElementCount(layerWithLeastZeros, 1, 2)
    println(result)
    printPicture(layers, 6, 25)
}

fun printPicture(layers: List<List<Int>>, rows: Int, cols: Int) {
    for (row in 0 until rows) {
        for (col in 0 until cols) {
            print(determinePixelValue(layers, row, col, cols))
        }
        println()
    }
}

fun determinePixelValue(layers: List<List<Int>>, row: Int, col: Int, cols: Int): String {
    for (layer in layers) {
        val pixelValue = determineLayerPixelValue(layer, row, col, cols)
        if (pixelValue != 2) {
            return if (pixelValue == 0) "." else "1"
        }
    }
    return "5"
}

fun determineLayerPixelValue(layer: List<Int>, row: Int, col: Int, cols: Int): Int {
    return layer[(row * cols) + col]
}


fun findLayerWithLeastZeros(layers: List<List<Int>>): List<Int> {
    var layerWithLeastZeros = layers[0]
    var zerosCount = layers[0].filter { it == 0 }.count()
    for (layer in layers) {
        val layerZerosCount = layer.filter { it == 0 }.count()
        if (layerZerosCount <= zerosCount) {
            zerosCount = layerZerosCount
            layerWithLeastZeros = layer
        }
    }
    return layerWithLeastZeros
}

fun calculateMultipleOfElementCount(layer: List<Int>, element1: Int, element2: Int): Int {
    val element1Count = layer.filter { it == element1 }.count()
    val element2Count = layer.filter { it == element2 }.count()
    return element1Count * element2Count
}