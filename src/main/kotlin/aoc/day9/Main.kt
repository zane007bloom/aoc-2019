package aoc.day9

import aoc.day7.IntCodeComputer
import aoc.day7.IntCodeComputerState
import shared.FileReader

fun main() {
    val codes = FileReader().readLongArray("day9_input.txt")
    println(getBoostKeyCode(mutableListOf(1), codes))
    println(getBoostKeyCode(mutableListOf(2), codes))
}

fun getBoostKeyCode(inputs: MutableList<Long>, codes: List<Long>): Long {
    val state = IntCodeComputerState(
        codes.toMutableList(),
        inputs,
        mutableListOf(),
        0,
        false,
        0,
        -1,
        0
    )
    val computer = IntCodeComputer(state)
    computer.runUntilDone()
    return state.outputs.last()
}