package aoc.day13

import aoc.day7.IntCodeComputer
import aoc.day7.IntCodeComputerState
import shared.FileReader

const val upRight = 0
const val downRight = 1
const val downLeft = 2
const val upLeft = 3

const val empty = 0
const val wall = 1
const val block = 2
const val paddle = 3
const val ball = 4

fun main() {
    val codes = FileReader().readLongArray("day13_input.txt")

    val newCodes = codes.toMutableList()
    newCodes[0] = 2

    val state = IntCodeComputerState(
        newCodes,
        mutableListOf(),
        mutableListOf(),
        0,
        false,
        0,
        -1,
        0
    )
    val computer = IntCodeComputer(state)

    computer.runUntilNOutputsOrDone(2442)
    val screen = Screen(buildImage(state.outputs), -1)
    printScreen(screen)


    state.outputs.clear()

    println("==============================")
    while (!state.isDone) {
        if (screen.image.values.contains(ball) && screen.image.values.contains(paddle)) {
            state.inputs.clear()
            val ballPos = screen.image.entries.filter { it.value == ball }.first().key
            val paddlePos = screen.image.entries.filter { it.value == paddle }.first().key
            when {
                paddlePos.x < ballPos.x -> state.inputs.add(1)
                paddlePos.x > ballPos.x -> state.inputs.add(-1)
                else -> state.inputs.add(0)
            }
        }
        computer.runUntilNOutputsOrDone(3)
        updateScreen(screen, state.outputs)
        printScreen(screen)
        state.outputs.clear()
    }

}

fun calculateBallCrossingPoints(screen: Screen): List<Point> {
    var ballPos = screen.image.entries.filter { it.value == 4 }.first().key
    var ballDirection = downRight
    val crossingPoints = mutableListOf<Point>()
    val tiles = screen.image.toMutableMap()
    val keysInPaddleRow = tiles.keys.filter { it.y == 20L }
    for (key in keysInPaddleRow) {
        tiles.put(key, wall)
    }
//    printScreen(Screen(tiles, 0))
    while (tiles.values.contains(block)) {
        val xTile = toX(ballDirection, ballPos)
        val yTile = toY(ballDirection, ballPos)
        val zTile = toZ(ballDirection, ballPos)
        ballDirection = newDirection(
            ballDirection,
            tiles[xTile]!!,
            tiles[yTile]!!,
            tiles.getOrDefault(zTile, 0)
        )
        destroyBlocks(tiles, yTile, xTile, zTile)
        tiles.put(ballPos, empty)
        ballPos = toZ(ballDirection, ballPos)
        tiles.put(ballPos, ball)
        if (ballPos.y == 19L) crossingPoints.add(ballPos)
//        printScreen(Screen(tiles, 0))
    }
    return crossingPoints
}

private fun destroyBlocks(
    tiles: MutableMap<Point, Int>,
    yTile: Point,
    xTile: Point,
    zTile: Point
) {
    if (tiles[xTile]!! != block
        && tiles[yTile]!! != block
        && tiles.getOrDefault(zTile, 0) == block
    ) {
        tiles.put(zTile, empty)
    }
    if (tiles[yTile]!! == block) {
        tiles.put(yTile, empty)
    }
    if (tiles[xTile]!! == block) {
        tiles.put(xTile, empty)
    }
}

fun newDirection(ballDirection: Int, objX: Int, objY: Int, objZ: Int): Int {
    if (bouncable(objX) && bouncable(objY)) {
        return oppositeDirection(ballDirection)
    }
    if (bouncable(objY)) {
        return when (ballDirection) {
            upRight -> downRight
            downRight -> upRight
            upLeft -> downLeft
            downLeft -> upLeft
            else -> throw RuntimeException("Unknown dir $ballDirection")
        }
    }
    if (bouncable(objX)) {
        return when (ballDirection) {
            upRight -> upLeft
            upLeft -> upRight
            downRight -> downLeft
            downLeft -> downRight
            else -> throw RuntimeException("Unknown dir $ballDirection")
        }
    }
    if (bouncable(objZ)) {
        return oppositeDirection(ballDirection)
    }
    return ballDirection
}

private fun oppositeDirection(direction: Int) = (direction + 2).rem(4)

fun bouncable(obj: Int): Boolean {
    return obj == wall || obj == block
}


fun toX(direction: Int, point: Point): Point {
    return when (direction) {
        upRight -> Point(point.x + 1, point.y)
        downRight -> Point(point.x + 1, point.y)
        downLeft -> Point(point.x - 1, point.y)
        upLeft -> Point(point.x - 1, point.y)
        else -> throw RuntimeException("??????")
    }
}


fun toY(direction: Int, point: Point): Point {
    return when (direction) {
        upRight -> Point(point.x, point.y - 1)
        downRight -> Point(point.x, point.y + 1)
        downLeft -> Point(point.x, point.y + 1)
        upLeft -> Point(point.x, point.y - 1)
        else -> throw RuntimeException("??????")
    }
}

fun toZ(direction: Int, point: Point): Point {
    return when (direction) {
        upRight -> Point(point.x + 1, point.y - 1)
        downRight -> Point(point.x + 1, point.y + 1)
        downLeft -> Point(point.x - 1, point.y + 1)
        upLeft -> Point(point.x - 1, point.y - 1)
        else -> throw RuntimeException("??????")
    }
}

fun updateScreen(screen: Screen, tiles: MutableList<Long>) {
    if (tiles.isEmpty()) {
        return
    }
    if (tiles[0] != -1L) {
        screen.image.put(Point(tiles[0], tiles[1]), tiles[2].toInt())
    } else {
        println("Updating Score")
        screen.score = tiles[2]
    }
}

fun buildImage(tiles: MutableList<Long>): MutableMap<Point, Int> {
    val screen = mutableMapOf<Point, Int>()
    for (i in 0 until tiles.size step 3) {
        if (tiles[i] != -1L) {
            screen.put(Point(tiles[i], tiles[i + 1]), tiles[i + 2].toInt())
        }
    }
    return screen
}

private fun printScreen(screen: Screen) {
    val sortedXComponents = screen.image.keys.map { it.x }.sorted()
    val sortedYComponents = screen.image.keys.map { it.y }.sorted()
    val startX = sortedXComponents.first()
    val endX = sortedXComponents.last()
    val startY = sortedYComponents.first()
    val endY = sortedYComponents.last()

    for (row in startY..endY) {
        for (col in startX..endX) {
            val tile = when (screen.image.getOrDefault(Point(col, row), 0)) {
                0 -> ' '
                1 -> '|'
                2 -> '#'
                3 -> '='
                4 -> 'o'
                else -> '?'
            }
            print(tile)
        }
        if (row == startY) {
            print(" ".repeat(10))
            print(screen.score)
        }
        println()
    }
}

data class Point(val x: Long, val y: Long)

data class Screen(val image: MutableMap<Point, Int>, var score: Long)