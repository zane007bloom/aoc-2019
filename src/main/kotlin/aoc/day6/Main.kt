package aoc.day6

import shared.FileReader

fun main() {
    val orbits = FileReader().readMap("day6_input.txt")

    val result = calculateOrbits(orbits)
    println(result.directOrbits + result.indirectOrbits)

    println(calculateMinOrbitalJumps(orbits))
}

fun calculateMinOrbitalJumps(orbits: Map<String, String>): Int {
    val root = constructTree(orbits)
    val youNode = findNode(root, "YOU")
    val santaNode = findNode(root, "SAN")
    val youParentNodes = findParentNodes(youNode)
    val santaParentNodes = findParentNodes(santaNode)
    val commonParents = santaParentNodes.intersect(youParentNodes).sortedByDescending { it.level }
    val deepestCommonParent = commonParents[0]
    return (youNode.level - 1 - deepestCommonParent.level) + (santaNode.level - 1 - deepestCommonParent.level)
}

fun findParentNodes(node: Node): List<Node> {
    var currentNode: Node? = node
    val parents = mutableListOf<Node>()

    while (currentNode != null) {
        parents.add(currentNode)
        currentNode = currentNode.parent
    }

    return parents
}

fun findNode(node: Node, name: String): Node {
    if (node.name == name) {
        return node
    }
    for (child in node.children) {
        try {
            return findNode(child, name)
        } catch (ex: RuntimeException) {

        }
    }
    throw RuntimeException("Not Found")
}

fun calculateOrbits(orbits: Map<String, String>): Orbits {

    val root = constructTree(orbits)

    return countOrbits(root, Orbits())
}

private fun countOrbits(node: Node, orbits: Orbits): Orbits {
    if (node.name == "COM") {
        node.children.forEach { countOrbits(it, orbits) }
    } else {
        orbits.directOrbits++
        orbits.indirectOrbits += (node.level - 1)
        node.children.forEach { countOrbits(it, orbits) }
    }
    return orbits
}

private fun constructTree(orbits: Map<String, String>): Node {
    val orbitNodes = mutableMapOf<String, Node>()
    orbits.forEach { (orbiter, orbitingAround) ->
        run {
            val orbitingNode = orbitNodes.getOrPut(orbiter, { Node(null, mutableListOf(), orbiter, -1) })
            val orbitingAroundNode =
                orbitNodes.getOrPut(orbitingAround, { Node(null, mutableListOf(), orbitingAround, -1) })
            orbitingAroundNode.children.add(orbitingNode)
            orbitingNode.parent = orbitingAroundNode
        }
    }
    val root = orbitNodes.getOrDefault("COM", Node(null, mutableListOf(), "UNKNOWN", -1))
    return calculateLevels(root, 0)
}

private fun calculateLevels(node: Node, currentLevel: Int): Node {
    node.level = currentLevel
    node.children.forEach { calculateLevels(it, currentLevel + 1) }
    return node
}

data class Node(var parent: Node?, val children: MutableList<Node>, val name: String, var level: Int) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Node

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    override fun toString(): String {
        return "Node(name='$name')"
    }
}

data class Orbits(var directOrbits: Int = 0, var indirectOrbits: Int = 0)