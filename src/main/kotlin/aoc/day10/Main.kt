package aoc.day10

import shared.FileReader
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import kotlin.math.abs
import kotlin.math.atan
import kotlin.math.max
import kotlin.math.min

fun main() {
    val points = FileReader().readPoints("day10_input.txt")
    println(bestNumberOfVisiblePoints(points.toMutableMap()))
    val point = determineAsteroidHit(200, points.toMutableMap())
    println(point.x * 100 + point.y)
}

data class Point(val x: Int, val y: Int)

fun bestNumberOfVisiblePoints(points: MutableMap<Point, Int>): Int {
    val summedPoints = determineNumberOfVisiblePoints(points)
    return summedPoints.values.max()!!
}

fun bestNumberOfVisiblePointsPoint(points: MutableMap<Point, Int>): Point {
    val summedPoints = determineNumberOfVisiblePoints(points)
    var bestPoint = points.keys.first()
    for (entry in summedPoints) {
        if (entry.value > points.getOrDefault(bestPoint, 0)) {
            bestPoint = entry.key
        }
    }
    return bestPoint
}

fun determineNumberOfVisiblePoints(points: MutableMap<Point, Int>): MutableMap<Point, Int> {
    for (entry in points) {
        val pointsToCount = points.keys.filter { it != entry.key }
        var visiblePoints = 0
        for (point in pointsToCount) {
            val pointsBetween = getPointsBetween(pointsToCount.filter { it != point }, entry.key, point)
            if (pointsBetween.isEmpty()) {
                visiblePoints++
            }
        }
        entry.setValue(visiblePoints)
    }
    return points
}

fun getVisiblePointsForPoint(point: Point, points: MutableMap<Point, Int>): List<Point> {
    val visiblePoints = mutableListOf<Point>()
    for (entry in points) {
        val pointsBetween = getPointsBetween(
            points.keys.filter { it != entry.key },
            entry.key,
            point
        )
        if (pointsBetween.isEmpty()) {
            visiblePoints.add(entry.key)
        }
    }
    return visiblePoints
}

fun getPointsBetween(points: List<Point>, point1: Point, point2: Point): List<Point> {
    return points.filter { isPointBetween(it, point1, point2) }
}

fun isPointBetween(point: Point, point1: Point, point2: Point): Boolean {
    if (point.y < min(point1.y, point2.y) || point.y > max(point1.y, point2.y)) {
        return false
    }
    if (point.x < min(point1.x, point2.x) || point.x > max(point1.x, point2.x)) {
        return false
    }
    if (point1.x == point2.x) {
        return point.x == point1.x
                && point.y > min(point1.y, point2.y)
                && point.y < max(point1.y, point2.y)
    }
    val m = (point2.y - point1.y).toBigDecimal()
        .multiply(BigDecimal(point.x - point1.x))
        .divide(BigDecimal(point2.x - point1.x), 5, RoundingMode.HALF_UP)
    return point.y.toBigDecimal().compareTo(m + point1.y.toBigDecimal()) == 0
}

fun determineAsteroidHit(n: Int, points: MutableMap<Point, Int>): Point {
    val center = bestNumberOfVisiblePointsPoint(determineNumberOfVisiblePoints(points))
    val vaporisedPoints = mutableListOf<Point>()
    val pointsRemaining = points.toMutableMap()
    pointsRemaining.remove(center)
    while (pointsRemaining.isNotEmpty()) {
        val visiblePoints = getVisiblePointsForPoint(center, pointsRemaining)
        val normalisePoints = normalisePoints(center, visiblePoints)
        val sortPoints = sortPoints(normalisePoints)
        val deNormalisePoints = deNormalisePoints(center, sortPoints)
        vaporisedPoints.addAll(deNormalisePoints)
        pointsRemaining.keys.removeAll(deNormalisePoints)
    }
    return vaporisedPoints[n - 1]
}

fun deNormalisePoints(center: Point, points: List<Point>): List<Point> {
    return points.map { deNormalisePoint(center, it) }
}

fun deNormalisePoint(center: Point, point: Point): Point {
    return Point(point.x + center.x, point.y * -1 + center.y)
}

fun normalisePoints(center: Point, points: List<Point>): List<Point> {
    return points.map { Point(it.x - center.x, (it.y - center.y) * -1) }
}

fun sortPoints(points: List<Point>): List<Point> {
    val sorted = points.toMutableList()
    sorted.sortWith(Comparator { p1, p2 ->
        angle(p1).compareTo(angle(p2))
    })
    return sorted
}

fun angle(point: Point): Double {
    if (point.x == 0) {
        if (point.y > 0) {
            return 0.0
        }
        if (point.y < 0) {
            return 180.0
        }
    }
    if (point.y == 0) {
        if (point.x > 0) {
            return 90.0
        }
        if (point.y < 0) {
            return 270.0
        }
    }
    val angle = Math.toDegrees(atan(abs(point.y).toDouble().div(abs(point.x))))
    return when (quadrant(point)) {
        0 -> 90 - angle
        1 -> 90 + angle
        2 -> 180 + (90 - angle)
        else -> 270 + angle
    }
}

fun quadrant(p: Point): Int {
    val x = p.x
    val y = p.y
    return when {
        x >= 0 && y >= 0 -> 0
        x >= 0 && y <= 0 -> 1
        x <= 0 && y <= 0 -> 2
        else -> 3
    }
}
