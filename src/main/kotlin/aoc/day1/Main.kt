package aoc.day1

import shared.FileReader

fun main() {
    val masses = FileReader().readFloatLines("day1_input.txt")
    println(Ship().calculateFuelRequired(masses))
    println(Ship().calculateFuelRequiredRecursively(masses))
}