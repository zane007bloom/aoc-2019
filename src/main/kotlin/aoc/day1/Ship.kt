package aoc.day1

class Ship {

    fun calculateFuelRequired(mass: Float): Int {
        return mass.div(3).toInt() - 2
    }

    fun calculateFuelRequired(masses: Collection<Float>): Int {
        return masses.map { calculateFuelRequired(it) }.sum()
    }

    fun calculateFuelRequiredRecursively(mass: Float): Int {
        val fuelRequired = calculateFuelRequired(mass)
        if (fuelRequired > 0) {
            return fuelRequired + calculateFuelRequiredRecursively(fuelRequired.toFloat())
        }
        return 0
    }

    fun calculateFuelRequiredRecursively(masses: Collection<Float>): Int {
        return masses.map { calculateFuelRequiredRecursively(it) }.sum()
    }

}