package aoc.day7

class IntCodeComputer(var state: IntCodeComputerState) {

    fun runUntilDone() {
        setPhase()

        while (!state.isDone) {
            runInstruction()
        }
    }

    fun runUntilNOutputsOrDone(n: Int) {
        setPhase()

        while (state.outputs.size != n && !state.isDone) {
            runInstruction()
        }
    }

    fun runUntilOutputOrDone() {
        setPhase()

        while (state.outputs.isEmpty() && !state.isDone) {
            runInstruction()
        }
    }

    private fun setPhase() {
        if (state.runCount == 0 && state.phase != -1) {
            state.inputs.add(0, state.phase.toLong())
        }
    }

    private fun runInstruction() {
        val instruction = state.codes[state.cursor]
        val missingLeadingZeros = 5 - instruction.toString().length
        val instructionString = "0".repeat(missingLeadingZeros) + instruction.toString()
        val param1Mode = instructionString[2]
        val param2Mode = instructionString[1]
        val param3Mode = instructionString[0]
        when (instructionString.substring(3)) {
            "01" -> {
                addInstruction(param1Mode, param2Mode, param3Mode)
            }
            "02" -> {
                multiplyInstruction(param1Mode, param2Mode, param3Mode)
            }
            "03" -> {
                readInputInstruction(param1Mode)
            }
            "04" -> {
                writeOutputInstruction(param1Mode)
            }
            "05" -> {
                jumpNotEqualInstruction(param1Mode, param2Mode)
            }
            "06" -> {
                jumpEqualInstruction(param1Mode, param2Mode)
            }
            "07" -> {
                lessThanInstruction(param1Mode, param2Mode, param3Mode)
            }
            "08" -> {
                equalInstruction(param1Mode, param2Mode, param3Mode)
            }
            "09" -> {
                increaseRelativeBase(param1Mode)
            }
            "99" -> {
                endInstruction()
            }
            else -> throw RuntimeException("Unsupported opcode: $instruction")
        }
        state.runCount++
    }

    private fun increaseRelativeBase(param1Mode: Char) {
        val value = readParam(1, param1Mode)
        state.relativeBase += value
        state.cursor += 2
    }

    private fun writeOutputInstruction(param1Mode: Char) {
        state.outputs.add(readParam(1, param1Mode))
        state.cursor += 2
    }

    private fun readInputInstruction(param1Mode: Char) {
        val value = state.inputs.removeAt(0)
        writeParam(1, param1Mode, value)
        state.cursor += 2
    }

    private fun multiplyInstruction(param1Mode: Char, param2Mode: Char, param3Mode: Char) {
        val param1 = readParam(1, param1Mode)
        val param2 = readParam(2, param2Mode)
        val value = param1 * param2
        writeParam(3, param3Mode, value)
        state.cursor += 4
    }

    private fun addInstruction(param1Mode: Char, param2Mode: Char, param3Mode: Char) {
        val param1 = readParam(1, param1Mode)
        val param2 = readParam(2, param2Mode)
        val value = param1 + param2
        writeParam(3, param3Mode, value)
        state.cursor += 4
    }

    private fun jumpNotEqualInstruction(param1Mode: Char, param2Mode: Char) {
        val param1 = readParam(1, param1Mode)
        val param2 = readParam(2, param2Mode)
        state.cursor = if (param1 != 0L) param2 else state.cursor + 3
    }

    private fun jumpEqualInstruction(param1Mode: Char, param2Mode: Char) {
        val param1 = readParam(1, param1Mode)
        val param2 = readParam(2, param2Mode)
        state.cursor = if (param1 == 0L) param2 else state.cursor + 3
    }

    private fun lessThanInstruction(param1Mode: Char, param2Mode: Char, param3Mode: Char) {
        val param1 = readParam(1, param1Mode)
        val param2 = readParam(2, param2Mode)
        if (param1 < param2)
            writeParam(3, param3Mode, 1)
        else
            writeParam(3, param3Mode, 0)
        state.cursor += 4
    }

    private fun equalInstruction(param1Mode: Char, param2Mode: Char, param3Mode: Char) {
        val param1 = readParam(1, param1Mode)
        val param2 = readParam(2, param2Mode)
        if (param1 == param2)
            writeParam(3, param3Mode, 1)
        else
            writeParam(3, param3Mode, 0)
        state.cursor += 4
    }

    private fun endInstruction() {
        state.isDone = true
    }

    private fun readParam(paramNumber: Int, paramMode: Char): Long {
        return state.codes.getOrDefault(determineParamIndex(paramNumber, paramMode), 0L)
    }

    private fun writeParam(paramNumber: Int, paramMode: Char, value: Long) {
        state.codes[determineParamIndex(paramNumber, paramMode)] = value
    }

    private fun determineParamIndex(paramNumber: Int, paramMode: Char): Long {
        return when (paramMode) {
            '0' -> state.codes.getOrDefault(state.cursor + paramNumber.toLong(), 0L)
            '1' -> state.cursor + paramNumber
            '2' -> state.codes.getOrDefault(state.cursor + paramNumber.toLong(), 0L) + state.relativeBase
            else -> throw RuntimeException("Cannot handle paramMode $paramMode")
        }
    }

}

class IntCodeComputerState(
    codeList: MutableList<Long>,
    var inputs: MutableList<Long>,
    var outputs: MutableList<Long>,
    var cursor: Long,
    var isDone: Boolean,
    var runCount: Int,
    val phase: Int,
    var relativeBase: Long
) {

    var codes: MutableMap<Long, Long> = mutableMapOf()

    init {
        codeList.forEachIndexed { index, l ->
            run {
                codes.put(index.toLong(), l)
            }
        }
    }
}