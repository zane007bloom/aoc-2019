package aoc.day7

import aoc.day5.Program
import shared.FileReader

fun main() {
    println(calcMaxThrusterSignal(FileReader().readIntArray("day7_input.txt")))
    println(calcMaxThrusterSignalWithFeedbackLoop(FileReader().readLongArray("day7_input.txt")))
}

fun calcMaxThrusterSignal(codes: List<Int>): Int {
    val outputs = mutableListOf<Int>()

    val phaseOptions = listOf(0, 1, 2, 3, 4)

    for (i in phaseOptions) {
        val used = mutableListOf<Int>()
        used.add(i)
        val jOptions = phaseOptions.toMutableList()
        jOptions.removeAll(used)
        for (j in jOptions) {
            used.add(j)
            val kOptions = phaseOptions.toMutableList()
            kOptions.removeAll(used)
            for (k in kOptions) {
                used.add(k)
                val lOptions = phaseOptions.toMutableList()
                lOptions.removeAll(used)
                for (l in lOptions) {
                    used.add(l)
                    val mOptions = phaseOptions.toMutableList()
                    mOptions.removeAll(used)
                    for (m in mOptions) {
                        outputs.add(calcMaxThrusterSignal(codes, listOf(i, j, k, l, m)))
                    }
                    used.remove(l)
                }
                used.remove(k)
            }
            used.remove(j)
        }
        used.remove(i)
    }

    outputs.sortDescending()
    return outputs[0]
}

fun calcMaxThrusterSignal(codes: List<Int>, phaseSettings: List<Int>): Int {
    var output = 0
    val program = Program(codes)
    for (i in 0 until 5) {
        val programOutput = program.run(mutableListOf(phaseSettings[i], output))
        output = programOutput.last()
    }
    return output
}

fun calcMaxThrusterSignalWithFeedbackLoop(codes: List<Long>): Long {
    val outputs = mutableListOf<Long>()

    val phaseOptions = listOf(5, 6, 7, 8, 9)

    for (i in phaseOptions) {
        val used = mutableListOf<Int>()
        used.add(i)
        val jOptions = phaseOptions.toMutableList()
        jOptions.removeAll(used)
        for (j in jOptions) {
            used.add(j)
            val kOptions = phaseOptions.toMutableList()
            kOptions.removeAll(used)
            for (k in kOptions) {
                used.add(k)
                val lOptions = phaseOptions.toMutableList()
                lOptions.removeAll(used)
                for (l in lOptions) {
                    used.add(l)
                    val mOptions = phaseOptions.toMutableList()
                    mOptions.removeAll(used)
                    for (m in mOptions) {
                        outputs.add(calcMaxThrusterSignalWithFeedbackLoop(codes, listOf(i, j, k, l, m)))
                    }
                    used.remove(l)
                }
                used.remove(k)
            }
            used.remove(j)
        }
        used.remove(i)
    }

    outputs.sortDescending()
    return outputs[0]
}

fun calcMaxThrusterSignalWithFeedbackLoop(codes: List<Long>, phaseSettings: List<Int>): Long {
    val programs = phaseSettings.map {
        IntCodeComputer(
            IntCodeComputerState(
                codes.toMutableList(),
                mutableListOf(),
                mutableListOf(),
                0,
                false,
                0,
                it,
                0
            )
        )
    }
    var outputs = mutableListOf(0L)
    var allOutputs = mutableListOf<Long>()
    while (!allProgramsDone(programs)) {
        programs.filter { !it.state.isDone }.forEach {
            run {
                val inputs = mutableListOf<Long>()
                inputs.addAll(outputs)
                it.state.inputs = inputs
                it.state.outputs.clear()
                it.runUntilOutputOrDone()
                outputs = it.state.outputs
                allOutputs.addAll(outputs)
            }
        }
    }
    return allOutputs.last()
}

fun allProgramsDone(programs: List<IntCodeComputer>): Boolean {
    return programs.none { !it.state.isDone }
}