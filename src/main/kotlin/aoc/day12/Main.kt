package aoc.day12

import shared.FileReader
import java.util.*
import kotlin.math.abs

fun main() {
    val moons = FileReader().readMoons("day12_input.txt")
    simulateMotion(8000, moons)
    val energy: Int = energy(moons)

    println(energy)

    println(leastStepsToAPreviousState(FileReader().readMoons("day12_input.txt")))
}

fun leastStepsToAPreviousState(moons: List<Moon>): Double {
    val previousXs = moons.map { it.position.x }
    val previousXVelocities = moons.map { it.velocity.x }
    val previousYs = moons.map { it.position.y }
    val previousYVelocities = moons.map { it.velocity.y }
    val previousZs = moons.map { it.position.z }
    val previousZVelocities = moons.map { it.velocity.z }

    var xCycle = -1
    var yCycle = -1
    var zCycle = -1
    var count = 0

    while (xCycle == -1 || yCycle == -1 || zCycle == -1) {
        count++
        simulateMotion(moons)
        if (xCycle == -1 && previousXs == moons.map { it.position.x }
            && previousXVelocities == moons.map { it.velocity.x }) {
            xCycle = count
        }
        if (yCycle == -1 && previousYs == moons.map { it.position.y }
            && previousYVelocities == moons.map { it.velocity.y }) {
            yCycle = count
        }
        if (zCycle == -1 && previousZs == moons.map { it.position.z }
            && previousZVelocities == moons.map { it.velocity.z }) {
            zCycle = count
        }
    }

    return lcm(listOf(xCycle.toDouble(), yCycle.toDouble(), zCycle.toDouble()))
}

private fun lcm(numbers: List<Double>): Double {
    return Arrays.stream(numbers.toDoubleArray()).reduce(1.0) { x, y -> x * (y.div(gcd(x, y))) }
}

private fun gcd(x: Double, y: Double): Double {
    return if (y == 0.0) x else gcd(y, x % y)
}


fun energy(moons: List<Moon>): Int {
    return moons.map { energy(it) }.sum()
}

fun energy(moon: Moon): Int {
    return (energy(moon.position)) * (energy(moon.velocity))
}

fun energy(point3D: Point3D): Int {
    return abs(point3D.x) + abs(point3D.y) + abs(point3D.z)
}

fun simulateMotion(n: Long, moons: List<Moon>) {
    for (i in 0 until n) {
        simulateMotion(moons)
    }
}

fun simulateMotion(moons: List<Moon>) {
    simulateGravity(moons)
    applyVelocity(moons)
}

private fun applyVelocity(moons: List<Moon>) {
    moons.forEach { it.position.addPoint3D(it.velocity) }
}

fun simulateGravity(moons: List<Moon>) {
    val uniquePairs = getUniquePairs(moons)
    uniquePairs.forEach { simulateGravity(it) }
}

fun simulateGravity(pair: Pair<Moon, Moon>) {
    if (pair.first.position.x > pair.second.position.x) {
        pair.first.velocity.x--
        pair.second.velocity.x++
    } else if (pair.first.position.x < pair.second.position.x) {
        pair.first.velocity.x++
        pair.second.velocity.x--
    }

    if (pair.first.position.y > pair.second.position.y) {
        pair.first.velocity.y--
        pair.second.velocity.y++
    } else if (pair.first.position.y < pair.second.position.y) {
        pair.first.velocity.y++
        pair.second.velocity.y--
    }

    if (pair.first.position.z > pair.second.position.z) {
        pair.first.velocity.z--
        pair.second.velocity.z++
    } else if (pair.first.position.z < pair.second.position.z) {
        pair.first.velocity.z++
        pair.second.velocity.z--
    }
}

fun <T> getUniquePairs(list: List<T>): List<Pair<T, T>> {
    val uniquePairs = mutableListOf<Pair<T, T>>()
    for (i in 0 until list.size) {
        for (j in i + 1 until list.size) {
            uniquePairs.add(Pair(list[i], list[j]))
        }
    }
    return uniquePairs
}

data class Point(val pos: Int, val velocity: Int)

class Point3D(var x: Int = 0, var y: Int = 0, var z: Int = 0) {

    fun addPoint3D(point3D: Point3D) {
        x += point3D.x
        y += point3D.y
        z += point3D.z
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Point3D

        if (x != other.x) return false
        if (y != other.y) return false
        if (z != other.z) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        result = 31 * result + z
        return result
    }

    override fun toString(): String {
        return "($x,$y,$z)"
    }

}

data class Moon(var position: Point3D, var velocity: Point3D)

fun clone(moons: List<Moon>): List<Moon> {
    return moons.map { clone(it) }
}

fun clone(moon: Moon): Moon {
    return Moon(
        Point3D(moon.position.x, moon.position.y, moon.position.z),
        Point3D(moon.velocity.x, moon.velocity.y, moon.velocity.z)
    )
}