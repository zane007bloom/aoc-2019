package aoc.day5

import shared.FileReader

fun main() {
    val codes = FileReader().readIntArray("day5_input.txt")
    val program = Program(codes)
    program.run(1)

    println()
    println("===============================")
    println()

    program.run(5)
}