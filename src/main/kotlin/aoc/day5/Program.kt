package aoc.day5

class Program(var codes: List<Int>) {

    private var inputs = mutableListOf<Int>()
    var outputs = mutableListOf<Int>()
    private var printOutput = false

    fun run(input: Int): List<Int> {
        printOutput = true
        this.inputs.add(input)
        return run(0, codes.toMutableList())
    }

    fun run(inputs: MutableList<Int>): List<Int> {
        outputs.clear()
        this.inputs.addAll(inputs)
        run(0, codes.toMutableList())
        return outputs
    }

    private fun run(cursor: Int, codes: MutableList<Int>): MutableList<Int> {
        return if (codes[cursor] != 99) {
            val newCursor = runOperation(cursor, codes)
            run(newCursor, codes)
        } else {
            codes
        }
    }

    private fun runOperation(cursor: Int, codes: MutableList<Int>): Int {
        val opCode = codes[cursor]
        val missingLeadingZeros = 5 - opCode.toString().length
        val opCodeString = "0".repeat(missingLeadingZeros) + opCode.toString()
        val param1Mode = opCodeString[2]
        val param2Mode = opCodeString[1]
        val param3Mode = opCodeString[0]
        when (opCodeString.substring(3)) {
            "01" -> {
                val param1 = if (param1Mode == '0') codes[codes[cursor + 1]] else codes[cursor + 1]
                val param2 = if (param2Mode == '0') codes[codes[cursor + 2]] else codes[cursor + 2]
                val param3 = codes[cursor + 3]
                codes[param3] = param1 + param2
                return cursor + 4
            }
            "02" -> {
                val param1 = if (param1Mode == '0') codes[codes[cursor + 1]] else codes[cursor + 1]
                val param2 = if (param2Mode == '0') codes[codes[cursor + 2]] else codes[cursor + 2]
                val param3 = codes[cursor + 3]
                codes[param3] = param1 * param2
                return cursor + 4
            }
            "03" -> {
                val param1 = codes[cursor + 1]
                codes[param1] = inputs.removeAt(0)
                return cursor + 2
            }
            "04" -> {
                val param1 = codes[cursor + 1]
                if (printOutput) println(codes[param1])
                outputs.add(codes[param1])
                return cursor + 2
            }
            "05" -> {
                val param1 = if (param1Mode == '0') codes[codes[cursor + 1]] else codes[cursor + 1]
                val param2 = if (param2Mode == '0') codes[codes[cursor + 2]] else codes[cursor + 2]
                return if (param1 != 0) param2 else cursor + 3
            }
            "06" -> {
                val param1 = if (param1Mode == '0') codes[codes[cursor + 1]] else codes[cursor + 1]
                val param2 = if (param2Mode == '0') codes[codes[cursor + 2]] else codes[cursor + 2]
                return if (param1 == 0) param2 else cursor + 3
            }
            "07" -> {
                val param1 = if (param1Mode == '0') codes[codes[cursor + 1]] else codes[cursor + 1]
                val param2 = if (param2Mode == '0') codes[codes[cursor + 2]] else codes[cursor + 2]
                val param3 = codes[cursor + 3]
                if (param1 < param2)
                    codes[param3] = 1
                else
                    codes[param3] = 0
                return cursor + 4
            }
            "08" -> {
                val param1 = if (param1Mode == '0') codes[codes[cursor + 1]] else codes[cursor + 1]
                val param2 = if (param2Mode == '0') codes[codes[cursor + 2]] else codes[cursor + 2]
                val param3 = codes[cursor + 3]
                if (param1 == param2)
                    codes[param3] = 1
                else
                    codes[param3] = 0
                return cursor + 4
            }
            else -> throw RuntimeException("Unsupported opcode: $opCode")
        }
    }

}