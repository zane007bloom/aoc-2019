package aoc.day3

import shared.FileReader
import kotlin.math.abs

fun main() {
    val wireDirections = FileReader().readArrays("day3_input.txt")
    println(calculateShortestCrossDistance(wireDirections))
    println(determineFewestSteps(wireDirections))
}

fun determineFewestSteps(wiresDirections: List<List<String>>): Int {
    val intersectionPoints: MutableSet<Pair<Int, Int>> = getIntersectionPoints(wiresDirections)
    return intersectionPoints.map { determineStepCount(it, wiresDirections) }.sortedBy { it }[0]
}

private fun determineStepCount(intersectionPoint: Pair<Int, Int>, wiresDirections: List<List<String>>): Int {
     return wiresDirections
         .map { determineCoordinates(it) }
         .map { it.indexOf(intersectionPoint) + 1 }
         .sum()
}

fun calculateShortestCrossDistance(wiresDirections: List<List<String>>): Int {
    val intersectionPoints: MutableSet<Pair<Int, Int>> = getIntersectionPoints(wiresDirections)
    val sortedCoordinates = intersectionPoints.sortedBy { calculateManhattanDistance(it) }
    return calculateManhattanDistance(sortedCoordinates[0])
}

private fun getIntersectionPoints(wiresDirections: List<List<String>>): MutableSet<Pair<Int, Int>> {
    var coordinates: MutableSet<Pair<Int, Int>> = mutableSetOf()
    val lineCoordinates = wiresDirections.map { determineCoordinates(it) }
    coordinates.addAll(lineCoordinates[0])
    lineCoordinates.forEach { coordinates = coordinates.intersect(it).toMutableSet() }
    return coordinates
}

private fun determineCoordinates(directions: List<String>): List<Pair<Int, Int>> {
    val coordinates: MutableList<Pair<Int, Int>> = mutableListOf()
    var start: Pair<Int, Int> = Pair(0, 0)
    directions.forEach {
        val generatedCoordinates = generateCoordinates(determineOperation(it), start)
        start = generatedCoordinates.last()
        coordinates.addAll(generatedCoordinates)
    }
    return coordinates
}

private fun generateCoordinates(operation: Operation, start: Pair<Int, Int>): List<Pair<Int, Int>> {
    val determineOperationFunction = determineOperationFunction(operation.direction)
    val coordinates: MutableList<Pair<Int, Int>> = mutableListOf()
    var currentCoordinate = start
    for (i in 0 until operation.count) {
        currentCoordinate = determineOperationFunction.invoke(currentCoordinate)
        coordinates.add(currentCoordinate)
    }
    return coordinates
}

private fun determineOperationFunction(direction: Char): (Pair<Int, Int>) -> Pair<Int, Int> {
    return when (direction) {
        'U' -> { input -> Pair(input.first, input.second + 1) }
        'R' -> { input -> Pair(input.first + 1, input.second) }
        'D' -> { input -> Pair(input.first, input.second - 1) }
        'L' -> { input -> Pair(input.first - 1, input.second) }
        else -> throw RuntimeException("Unknown direction $direction")
    }
}

private fun determineOperation(operation: String): Operation {
    return Operation(operation[0], operation.substring(1).toInt())
}

private fun calculateManhattanDistance(coordinate: Pair<Int, Int>): Int {
    return abs(coordinate.first) + abs(coordinate.second)
}

data class Operation(val direction: Char, val count: Int)