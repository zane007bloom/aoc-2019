package aoc.day4

fun main() {
    println(calculatePossiblePasswords().count())
    println(calculatePossiblePasswordsWithGroups().count())
}

private fun calculatePossiblePasswords() = (124075..580769).filter { meetsCriteria(it.toString()) }

private fun calculatePossiblePasswordsWithGroups() = (124075..580769).filter { meetsCriteriaWithGroups(it.toString()) }

fun meetsCriteria(password: String): Boolean {
    var hasDouble = false

    for (i in 1 until password.length) {
        if (password[i] == password[i-1]) hasDouble = true
        else if (password[i] < password[i-1]) return false
    }

    return hasDouble
}

fun meetsCriteriaWithGroups(password: String): Boolean {
    var hasDouble = false

    for (i in 1 until password.length) {
        if (password[i] == password[i-1] && (i-2 < 0 || password[i-2] != password[i]) && (i+1 >= password.length || password[i+1] != password[i])) hasDouble = true
        else if (password[i] < password[i-1]) return false
    }

    return hasDouble
}