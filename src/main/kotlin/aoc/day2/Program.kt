package aoc.day2

class Program(var codes: List<Int>) {

    fun determineNounVerbValue(targetValue: Int): Int {
        for (noun in 0 until 100) {
            for (verb in 0 until 100) {
                resetAlarmState(noun, verb)
                val output = run(codes, 0)
                if (output[0] == targetValue) return 100 * noun + verb
            }
        }
        return -1
    }

    fun resetAlarmState(index1Val: Int, index2Val: Int) {
        val newCodes = codes.toIntArray()
        newCodes[1] = index1Val
        newCodes[2] = index2Val
        codes = newCodes.asList()
    }

    fun run(): List<Int> {
        return run(codes, 0)
    }

    private fun run(codes: List<Int>, operationIndex: Int): List<Int> {
        return if (codes[operationIndex] != 99) {
            val inputAIndex = codes[operationIndex + 1]
            val inputBIndex = codes[operationIndex + 2]
            val outputIndex = codes[operationIndex + 3]
            val operation = determineOperation(codes[operationIndex])
            val output = operation.invoke(codes[inputAIndex], codes[inputBIndex])
            val newProgram = codes.toIntArray()
            newProgram[outputIndex] = output
            run(newProgram.asList(), operationIndex + 4)
        } else {
            codes
        }
    }

    private fun determineOperation(opCode: Int): (Int, Int) -> Int {
        return when (opCode) {
            1 -> { a: Int, b: Int -> a + b }
            2 -> { a: Int, b: Int -> a * b }
            else -> throw RuntimeException("Unsupported opcode: $opCode")
        }
    }

}