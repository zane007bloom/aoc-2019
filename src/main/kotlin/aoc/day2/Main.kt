package aoc.day2

import shared.FileReader

fun main() {
    val codes = FileReader().readIntArray("day2_input.txt")
    val program = Program(codes)
    program.resetAlarmState(12, 2)
    println(program.run())
    println(program.determineNounVerbValue(19690720))
}