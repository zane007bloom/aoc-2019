package shared

import aoc.day10.Point
import aoc.day12.Moon
import aoc.day12.Point3D
import java.io.File

class FileReader {

    fun readLines(filename: String): List<String> {
        val file = getFile(filename)
        return file.readLines()
    }


    fun readFloatLines(filename: String): List<Float> {
        return readLines(filename).map { it.toFloat() }
    }

    fun readIntArray(filename: String): List<Int> {
        return getFile(filename).readText().split(',').map { it.toInt() }
    }

    fun readLongArray(filename: String): List<Long> {
        return getFile(filename).readText().split(',').map { it.toLong() }
    }

    fun readArrays(filename: String): List<List<String>> {
        return getFile(filename).readLines().map { it.split(',') }
    }

    fun readArrays(filename: String, size: Int): List<List<Int>> {
        val text = getFile(filename).readText()
        val arrays = mutableListOf<List<Int>>()
        for (i in 0 until (text.length / size)) {
            arrays.add(text.substring(i * size, i * size + size).map { it.toString().toInt() })
        }
        return arrays
    }

    fun readMap(filename: String): Map<String, String> {
        return getFile(filename).readLines()
            .map { it.split(')')[1] to it.split(')')[0] }
            .toMap()
    }

    fun readPoints(filename: String): Map<Point, Int> {
        val pairs = mutableMapOf<Point, Int>()
        getFile(filename)
            .readLines()
            .mapIndexed { y, line -> line.forEachIndexed { x, point -> if (point == '#') pairs.put(Point(x, y), 0) } }
        return pairs
    }

    fun readMoons(filename: String): List<Moon> {
        val pairs = mutableMapOf<Point, Int>()
        return getFile(filename)
            .readLines()
            .map { line -> line.filter { !listOf('<', '>', '=', ' ', 'x', 'y', 'z').contains(it) } }
            .map { it.split(",") }
            .map { Moon(Point3D(it[0].toInt(), it[1].toInt(), it[2].toInt()), Point3D()) }
    }

    private fun getFile(filename: String): File {
        val resource = FileReader::class.java.classLoader.getResource(filename)
        return File(resource.toURI())
    }
}