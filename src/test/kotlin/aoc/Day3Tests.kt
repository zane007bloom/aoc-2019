package aoc

import aoc.day3.calculateShortestCrossDistance
import aoc.day3.determineFewestSteps
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class Day3Tests {

    @Test
    fun shortestCrossDistance() {
        val distance = calculateShortestCrossDistance(
            listOf(
                listOf("R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"),
                listOf("U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83")
            )
        )
        Assertions.assertThat(distance).isEqualTo(159)
    }

    @Test
    fun shortestCrossDistance_longWires() {
        val distance = calculateShortestCrossDistance(
            listOf(
                listOf("R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"),
                listOf("U98","R91","D20","R16","D67","R40","U7","R15","U6","R7")
            )
        )
        Assertions.assertThat(distance).isEqualTo(135)
    }

    @Test
    fun fewestSteps() {
        val steps = determineFewestSteps(
            listOf(
                listOf("R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"),
                listOf("U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83")
            )
        )
        Assertions.assertThat(steps).isEqualTo(610)
    }

    @Test
    fun fewestSteps_longWires() {
        val steps = determineFewestSteps(
            listOf(
                listOf("R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"),
                listOf("U98","R91","D20","R16","D67","R40","U7","R15","U6","R7")
            )
        )
        Assertions.assertThat(steps).isEqualTo(410)
    }
}