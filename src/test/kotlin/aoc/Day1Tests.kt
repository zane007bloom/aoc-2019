package aoc

import aoc.day1.Ship
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class Day1Tests {

    @Test
    fun basic_12Mass() {
        val fuelRequired = Ship().calculateFuelRequired(12F)
        assertThat(fuelRequired).isEqualTo(2)
    }

    @Test
    fun basic_14Mass() {
        val fuelRequired = Ship().calculateFuelRequired(14F)
        assertThat(fuelRequired).isEqualTo(2)
    }

    @Test
    fun basic_1969Mass() {
        val fuelRequired = Ship().calculateFuelRequired(1969F)
        assertThat(fuelRequired).isEqualTo(654)
    }

    @Test
    fun basic_100756Mass() {
        val fuelRequired = Ship().calculateFuelRequired(100756F)
        assertThat(fuelRequired).isEqualTo(33583)
    }

    @Test
    fun manyMasses() {
        val fuelRequired = Ship().calculateFuelRequired(listOf(12F, 14F, 1969F, 100756F))
        assertThat(fuelRequired).isEqualTo(34241)
    }

    @Test
    fun basic_14MassRecursive() {
        val fuelRequired = Ship().calculateFuelRequiredRecursively(14F)
        assertThat(fuelRequired).isEqualTo(2)
    }

    @Test
    fun basic_1969MassRecursive() {
        val fuelRequired = Ship().calculateFuelRequiredRecursively(1969F)
        assertThat(fuelRequired).isEqualTo(966)
    }

    @Test
    fun basic_100756MassRecursive() {
        val fuelRequired = Ship().calculateFuelRequiredRecursively(100756F)
        assertThat(fuelRequired).isEqualTo(50346)
    }

    @Test
    fun manyMassesRecursive() {
        val fuelRequired = Ship().calculateFuelRequiredRecursively(listOf(14F, 1969F))
        assertThat(fuelRequired).isEqualTo(968)
    }

}