package aoc

import aoc.day2.Program
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class Day2Tests {

    @Test
    fun basic_program1() {
        val output = Program(listOf(1, 0, 0, 0, 99)).run()
        Assertions.assertThat(output).isEqualTo(listOf(2, 0, 0, 0, 99))
    }

    @Test
    fun basic_program2() {
        val output = Program(listOf(2, 3, 0, 3, 99)).run()
        Assertions.assertThat(output).isEqualTo(listOf(2, 3, 0, 6, 99))
    }

    @Test
    fun medium_program() {
        val output = Program(listOf(2, 4, 4, 5, 99, 0)).run()
        Assertions.assertThat(output).isEqualTo(listOf(2, 4, 4, 5, 99, 9801))
    }

    @Test
    fun complex_program() {
        val output = Program(listOf(1, 1, 1, 4, 99, 5, 6, 0, 99)).run()
        Assertions.assertThat(output).isEqualTo(listOf(30, 1, 1, 4, 2, 5, 6, 0, 99))
    }

    @Test
    fun resetTo1202AlarmState() {
        val program = Program(listOf(1, 1, 1, 4))
        program.resetAlarmState(12, 2)
        Assertions.assertThat(program.codes).isEqualTo(listOf(1, 12, 2, 4))
    }

}