package aoc

import aoc.day7.IntCodeComputer
import aoc.day7.IntCodeComputerState
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class Day9Tests {

    @Test
    fun relativeBase_outputCodes() {
        val codes = mutableListOf<Long>(109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99)
        val state = IntCodeComputerState(
            codes,
            mutableListOf(),
            mutableListOf(),
            0,
            false,
            0,
            -1,
            0
        )
        val computer = IntCodeComputer(state)
        computer.runUntilDone()
        Assertions.assertThat(computer.state.outputs).isEqualTo(codes)
    }

    @Test
    fun relativeBase_16DigitOutput() {
        val codes = mutableListOf<Long>(1102, 34915192, 34915192, 7, 4, 7, 99, 0)
        val state = IntCodeComputerState(
            codes,
            mutableListOf(),
            mutableListOf(),
            0,
            false,
            0,
            -1,
            0
        )
        val computer = IntCodeComputer(state)
        computer.runUntilDone()
        Assertions.assertThat(computer.state.outputs).isEqualTo(mutableListOf(1219070632396864))
    }

    @Test
    fun relativeBase_1125899906842624() {
        val codes = mutableListOf<Long>(104, 1125899906842624, 99)
        val state = IntCodeComputerState(
            codes,
            mutableListOf(),
            mutableListOf(),
            0,
            false,
            0,
            -1,
            0
        )
        val computer = IntCodeComputer(state)
        computer.runUntilDone()
        Assertions.assertThat(computer.state.outputs).isEqualTo(mutableListOf(1125899906842624))
    }

}