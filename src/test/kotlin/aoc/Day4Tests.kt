package aoc

import aoc.day4.meetsCriteria
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class Day4Tests {

    @Test
    fun input_111111() {
        val output = meetsCriteria("111111")
        Assertions.assertThat(output).isEqualTo(true)
    }

    @Test
    fun input_223450() {
        val output = meetsCriteria("223450")
        Assertions.assertThat(output).isEqualTo(false)
    }

    @Test
    fun input_123789() {
        val output = meetsCriteria("123789")
        Assertions.assertThat(output).isEqualTo(false)
    }

}