package aoc

import aoc.day5.Program
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import shared.FileReader

class Day5Tests {

    @Test
    fun program() {
        val codes = FileReader().readIntArray("day5_input.txt")

        val program = Program(codes)

        program.run(1)
        Assertions.assertThat(program.outputs.last()).isEqualTo(11933517)

        program.run(5)
        Assertions.assertThat(program.outputs.last()).isEqualTo(10428568)
    }

}