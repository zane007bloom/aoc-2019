package aoc

import aoc.day12.*
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import shared.FileReader


class Day12Tests {

    @Test
    fun readMoons() {
        val moons = FileReader().readMoons("day12_test_easy.txt")

        Assertions.assertThat(moons).isEqualTo(
            listOf(
                Moon(Point3D(-1, 0, 2), Point3D()),
                Moon(Point3D(2, -10, -7), Point3D()),
                Moon(Point3D(4, -8, 8), Point3D()),
                Moon(Point3D(3, 5, -1), Point3D())
            )
        )
    }

    @Test
    fun simulateOneTimeStep() {
        val moons = FileReader().readMoons("day12_test_easy.txt")

        simulateMotion(moons)

        Assertions.assertThat(moons).isEqualTo(
            listOf(
                Moon(Point3D(2, -1, 1), Point3D(3, -1, -1)),
                Moon(Point3D(3, -7, -4), Point3D(1, 3, 3)),
                Moon(Point3D(1, -7, 5), Point3D(-3, 1, -3)),
                Moon(Point3D(2, 2, 0), Point3D(-1, -3, 1))
            )
        )
    }

    @Test
    fun simulate10TimeStep() {
        val moons = FileReader().readMoons("day12_test_easy.txt")

        simulateMotion(10, moons)

        Assertions.assertThat(moons).isEqualTo(
            listOf(
                Moon(Point3D(2, 1, -3), Point3D(-3, -2, 1)),
                Moon(Point3D(1, -8, 0), Point3D(-1, 1, 3)),
                Moon(Point3D(3, -6, 1), Point3D(3, 2, -3)),
                Moon(Point3D(2, 0, 4), Point3D(1, -1, -1))
            )
        )

    }

    @Test
    fun energyAfter10Steps() {
        val moons = FileReader().readMoons("day12_test_easy.txt")

        simulateMotion(10, moons)

        val energy: Int = energy(moons)

        Assertions.assertThat(energy).isEqualTo(179)
    }

    @Test
    fun leastStepsToAPreviousState_easy() {
        val moons = FileReader().readMoons("day12_test_easy.txt")

        Assertions.assertThat(leastStepsToAPreviousState(moons)).isEqualTo(2772.0)
    }

    @Test
    fun leastStepsToAPreviousState_hard() {
        val moons = FileReader().readMoons("day12_test_hard.txt")

        Assertions.assertThat(leastStepsToAPreviousState(moons)).isEqualTo(4686774924.0)
    }

}

