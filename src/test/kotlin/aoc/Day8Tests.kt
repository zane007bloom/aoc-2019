package aoc

import aoc.day8.calculateMultipleOfElementCount
import aoc.day8.findLayerWithLeastZeros
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import shared.FileReader

class Day8Tests {

    @Test
    fun readArrays() {
        val layers = FileReader().readArrays("day8_test_input.txt", 6)
        Assertions.assertThat(layers).isEqualTo(
            listOf(
                listOf(1, 2, 3, 4, 5, 6),
                listOf(7, 8, 9, 0, 1, 2)
            )
        )
    }

    @Test
    fun findLayerWithLeastZeros() {
        val layers = FileReader().readArrays("day8_test_input.txt", 6)
        val layer = findLayerWithLeastZeros(layers)
        Assertions.assertThat(layer).isEqualTo(listOf(1, 2, 3, 4, 5, 6))
    }

    @Test
    fun calculateMultipleOfElementCount() {
        val layers = FileReader().readArrays("day8_test_input.txt", 6)
        val layer = findLayerWithLeastZeros(layers)
        val result = calculateMultipleOfElementCount(layer, 0, 1)
        Assertions.assertThat(result).isEqualTo(0)
    }

}