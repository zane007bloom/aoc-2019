package aoc

import aoc.day10.Point
import aoc.day11.runRobot
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import shared.FileReader

class Day11Tests {

    @Test
    fun runRobot() {
        val codes = FileReader().readLongArray("day11_test_input.txt")
        val panels = mutableMapOf<Point, Int>()

        runRobot(codes, panels)

        Assertions.assertThat(panels.size).isEqualTo(2093)
    }

}

