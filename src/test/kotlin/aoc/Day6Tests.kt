package aoc

import aoc.day6.calculateMinOrbitalJumps
import aoc.day6.calculateOrbits
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class Day6Tests {

    @Test
    fun orbitsTest() {
        val orbits = mapOf(
            "B" to "COM",
            "C" to "B",
            "D" to "C",
            "E" to "D",
            "F" to "E",
            "G" to "B",
            "H" to "G",
            "I" to "D",
            "J" to "E",
            "K" to "J",
            "L" to "K"
        )

        val (directOrbits, indirectOrbits) = calculateOrbits(orbits)

        Assertions.assertThat(directOrbits + indirectOrbits).isEqualTo(42)
    }

    @Test
    fun orbitalJumpsTest() {
        val orbits = mapOf(
            "B" to "COM",
            "C" to "B",
            "D" to "C",
            "E" to "D",
            "F" to "E",
            "G" to "B",
            "H" to "G",
            "I" to "D",
            "J" to "E",
            "K" to "J",
            "L" to "K",
            "YOU" to "K",
            "SAN" to "I"
        )

        val jumps = calculateMinOrbitalJumps(orbits)

        Assertions.assertThat(jumps).isEqualTo(4)
    }

}