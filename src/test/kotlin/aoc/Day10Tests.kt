package aoc

import aoc.day10.Point
import aoc.day10.bestNumberOfVisiblePoints
import aoc.day10.determineAsteroidHit
import aoc.day10.determineNumberOfVisiblePoints
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import shared.FileReader

class Day10Tests {

    @Test
    fun readPoints() {
        val points = FileReader().readPoints("day10_test_input.txt")

        Assertions.assertThat(points.size).isEqualTo(10)

        Assertions.assertThat(points.keys).contains(Point(1, 0))
        Assertions.assertThat(points.keys).contains(Point(4, 0))
        Assertions.assertThat(points.keys).contains(Point(0, 2))
        Assertions.assertThat(points.keys).contains(Point(1, 2))
        Assertions.assertThat(points.keys).contains(Point(2, 2))
        Assertions.assertThat(points.keys).contains(Point(3, 2))
        Assertions.assertThat(points.keys).contains(Point(4, 2))
        Assertions.assertThat(points.keys).contains(Point(4, 3))
        Assertions.assertThat(points.keys).contains(Point(3, 4))
        Assertions.assertThat(points.keys).contains(Point(4, 4))
    }

    @Test
    fun determineNumberOfVisiblePoints_allPoints() {
        var points = FileReader().readPoints("day10_test_input.txt")

        points = determineNumberOfVisiblePoints(points.toMutableMap())

        Assertions.assertThat(points.getOrDefault(Point(1, 0), 0)).isEqualTo(7)
        Assertions.assertThat(points.getOrDefault(Point(4, 0), 0)).isEqualTo(7)
        Assertions.assertThat(points.getOrDefault(Point(0, 2), 0)).isEqualTo(6)
        Assertions.assertThat(points.getOrDefault(Point(1, 2), 0)).isEqualTo(7)
        Assertions.assertThat(points.getOrDefault(Point(2, 2), 0)).isEqualTo(7)
        Assertions.assertThat(points.getOrDefault(Point(3, 2), 0)).isEqualTo(7)
        Assertions.assertThat(points.getOrDefault(Point(4, 2), 0)).isEqualTo(5)
        Assertions.assertThat(points.getOrDefault(Point(4, 3), 0)).isEqualTo(7)
        Assertions.assertThat(points.getOrDefault(Point(3, 4), 0)).isEqualTo(8)
        Assertions.assertThat(points.getOrDefault(Point(4, 4), 0)).isEqualTo(7)
    }

    @Test
    fun bestNumberOfVisiblePoints_33() {
        val points = FileReader().readPoints("day10_test_33_input.txt")

        val bestNumberOfVisiblePoints = bestNumberOfVisiblePoints(points.toMutableMap())

        Assertions.assertThat(bestNumberOfVisiblePoints).isEqualTo(33)
    }

    @Test
    fun asteroid200() {
        val points = FileReader().readPoints("day10_test_802_input.txt")

        val point = determineAsteroidHit(200, points.toMutableMap())

        Assertions.assertThat(point).isEqualTo(Point(8, 2))
    }

}

